Cours de R à destination des internes de santé publique
================

Présentation
------------

Bienvenue sur le repository regroupant les cours R dédiés aux internes de Santé Publique de Paris.

Programme
---------

Pour le moment, 4 cours sont au programme :

1.  Les fonctions.
2.  Les boucles et les \*apply.
3.  Introduction au Hadleyverse : dplyr, tidyr, etc.
4.  Les expressions régulières.

D'autres cours sont prévus : leur existence et leur contenu seront en fonction du succès et des retours des internes (ex : Shiny, RMarkdown, etc.).

Modalités pratiques
-------------------

Les premiers cours auront lieu au mois de Juin 2016 (date/heure/lieux seront précisés par mail aux inscrits).

L'ensemble des slides (réalisés sous R, via RMarkdown) sont téléchargeables sur le repository.
